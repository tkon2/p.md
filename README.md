# p.md - Presentation tool with markdown

Write simple markdown and convert it into text-only presentations in HTML.

Points are revealed individually. Once all points on a slide are revealed, the 
next button press will switch to the next slide. The exception to this is 
subtitles, which are revealed right away. 

Whitespace does not matter, you can make your document as cramped as you like. 
The only exception to this is the space after the # on the first line of each
slide. The space can be left out if there is no title however. 

## Format

```md
# First slide

## Subtitle

# Second slide

* Point 1
* Point 2
```

That's it!

The rules:

* Any line with a # followed by a space or a newline denotes a new slide
* Any other headers are considered subtitles with decreasing size
* Lines with * as the first non-whitespace character are points

## Compilation

Provide the file as the first and only parameter, the compiled HTML will be 
emitte on STDOUT, so pipe it wherever you like. 

