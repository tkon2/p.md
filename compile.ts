import fs from 'fs';

type Item = string;
type Slide = Item[];


const getLevel = (str: string) => {
  for (let i = 0; i < str.length; i++) {
    if (str.charAt(i) !== '#') return i;
  }
  return 0;
}

const item = (line: string, slideNumber: number, itemNumber: number) => {
  const content = line.replace(/^[*#]* */, '');
  if (line.replace(/ *$/, '') === '#') return '';
  const idStr = `id="item-${slideNumber}-${itemNumber}"`;
  if (line.startsWith('#')) {
    const level = getLevel(line);
    return (`
      <h${level} ${idStr}>${content}</h${level}>`);
  }
  if (line.startsWith('![')) return (`
      <img ${idStr} src="${line.replace(/.*\((.*)\).*/, "$1")}"></img>`);
  if (line.startsWith('*')) return (`
      <p ${idStr}>${content}</p>`);
  else return (`
      <span ${idStr}>${content}</span>`);
};

const slide = (slide: Slide, slideNumber: number) => (`
    <section id="slide-${slideNumber}">${
      slide.map((d, i) => item(d, slideNumber, i)).join('')}
    </section>`
);

const html = (slides: Slide[]) => {
  let slid: [string, string][] = [];

  for (let s = 0; s < slides.length; s++) {
    const slide = slides[s];
      slid.push([`"slide-${s}"`,`""`]);
    for (let i = 0; i < slide.length; i++) {
      const item = slide[i];
      if (item.startsWith('*') || item.startsWith('![')) slid.push([`"slide-${s}"`,`"item-${s}-${i}"`]);
    }
  }

  return (
`<!DOCTYPE html>
<html>
  <head>
    <style>
      html, body { background-color: #000; overflow: hidden; padding: 0; margin: 0; font-family: sans-serif; }
      * { transition: color 0.5s ease-out, border-color 0.5s ease-out; padding: 0; margin: 0; }
      section { position: absolute; width: 100vw; height: 100vh; left: 100vw; top: 0; transition: top 0.5s ease-out, left 0.5s ease-out; display: flex; flex-direction: column; align-items: flex-start; justify-content: flex-start; gap: 1vh; padding: 10vh 10vw; box-sizing: border-box; }
      section:first-child { align-items: center; justify-content: center; padding: 0; }
      section:first-child>h1 { padding-left: 2vw; }
      h1 { font-size: 1.7vmax; font-weight: normal; margin-bottom: 3vh; padding-right: 2vw; padding-bottom: 0.3vh; border-bottom-width: 0.2vh; border-bottom-style: solid; }
      h2 { font-size: 3vmax; }
      h3 { font-size: 4vmax; }
      p { font-size: 3.5vmax; list-style-type: circle; }
      p::before { content: "▪ "; display: inline-block; transform: translateY(0.1vmax); font-size: 3.5vmax; padding-right: 1vw; }
      section.past { left: 100vw; left: 0; }
      section.past * { color: black; border-color: black; }
      section.current { left: 0; }
      section.future { left: -100vw; }
      .past { color: rgb(70, 70, 70); border-color: rgb(70, 70, 70); }
      .current { color: white; border-color: white; }
      .future { color: black; border-color: black; }
      img { max-width: 40vw; max-height: 80vh; position: absolute; top: 0; right: 0; padding: 10vh 10vw; transition: filter 0.5s; }
      img.past { filter: opacity(80%); }
      img.future { filter: opacity(0%); }
      section.past img { filter: opacity(0%); }
      img.img-past { filter: opacity(0%); }
    </style>
  </head>
  <body>${slides.map((s, i) => slide(s, i)).join('')}
  <script>
    let index = 0;
    const slides = [
${slid.map(s => `      [${s[0]}, ${s[1]}]`).join(',\n')}
    ];
    const setIndex = (newIndex) => {
      const i = Math.max(0, Math.min(newIndex, slides.length - 1));
      index = i;
      const item = slides[i];
      const slideIndex = parseInt(item[0].replace('slide-',''));
      slides.forEach((s, slideNr) => {
        if (s[1] === '') {
          const currentSlideIndex = parseInt(s[0].replace('slide-',''));
          let cn = "current";
          if (currentSlideIndex < slideIndex) cn = "past";
          if (currentSlideIndex > slideIndex) cn = "future";
          document.getElementById(s[0]).className = cn;
        } else {
          let cn = "current";
          if (slideNr < i) cn = "past";
          if (slideNr > i) cn = "future";
          document.getElementById(s[1]).className = cn;
        }
      });

      let found = false;
      [...slides].reverse().forEach((s, rawSlideNr) => {
        const slideNr = (slides.length - 1) - rawSlideNr;
        let cn = found ? "img-past" : "img-future";
        const el = document.getElementById(s[1]);
        if (!el) return;
        if (el.nodeName === 'IMG') {
          if (slideNr <= i && !found) {
            found = true;
            cn = 'img-current';
          }
          el.className += " " + cn;
        }
      })
    };

    document.addEventListener("DOMContentLoaded", () => {
      setIndex(0);
    });

    document.addEventListener("keydown", (e) => {
      // Next
      const n = () => setIndex(index+1);
      // Previous
      const p = () => setIndex(index-1);

      switch (e.key) {
        case 'n':
        case 'ArrowRight':
        case 'ArrowDown':
        case ' ':
        case 'Enter': {
          n();
          break;
        }
        case 'p':
        case 'ArrowLeft':
        case 'ArrowUp':
        case 'Backspace': {
          p();
          break;
        }
      }
    });
  </script>
  </body>
</html>`
  );
}

/** Utility function to get the slides as a 2D array ([slide][item]) */
const getSections = (md: string) => {
  // Get lines and filter out empty lines
  const lines = md.split('\n')
    .filter(l => !/^ *$/.test(l));

  const sections: string[][] = [];
  let last: number = lines.length;
  for (let i = lines.length - 1; i >= 0; i--) {
    const l = lines[i];
    if (/^ *#( |$)/.test(l)) {
      sections.unshift(lines.slice(i, last).map(l => l.replace(/^ */, '')));
      last = i;
    }
  }
  return sections as Slide[];
}

export const compile = (md: string) => {
  const slides = getSections(md);
  return html(slides);
};

if (require.main === module) {
  const filename = process.argv[process.argv.length-1] || '';
  if (!filename.endsWith(".md")) {
    console.error("File must be markdown");
    process.exit(1);
  }

  fs.readFile(filename, 'utf8', (err, data) => {
    if (err) {
      console.error("File not found");
      return process.exit(1);
    } else {
      const html = compile(data);
      console.log(html);
    }
  });
}

